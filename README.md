# Objectifs

Ce projet est un template pour le développement de nouveaux playbooks au sein de
CIM.

## Struture du template


## Provisionner des machines de tests en local

Vous avez à votre disposition un fichier Vagrantfile que vous pouvez adapter à
votre besoin. Le playbook playbooks/provision-playbook va configurer ces
machines pour qu'elles facilement accessible. Il va en effet activer openssh et
copier votre clé ssh dans le fichier authorized_keys du user vagrant.
Tous les playbooks lancés sur ces machines devront être fait avec le user vagrant.

```bash
ansible-playbook -i inventories/test -u vagrant playbooks/first.yml
```

## testing

https://testinfra.readthedocs.io/en/latest/modules.html
py.test --hosts=webservers --connection=ansible --ansible-inventory=inventories/ --force-ansible -v tests/webservers.py